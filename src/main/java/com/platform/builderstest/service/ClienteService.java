package com.platform.builderstest.service;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;  
import com.platform.builderstest.model.Cliente;
import com.platform.builderstest.repository.ClienteRepository;

@Service
public class ClienteService {
    @Autowired
    ClienteRepository clienteRepository;

    public List<Cliente> getAllClientes() {       
        List<Cliente> clientes = new ArrayList<Cliente>();  
        clienteRepository.findAll().forEach(cliente -> clientes.add(cliente));  
        return clientes;  
    }     

    public Cliente getClienteById(int id) {  
        return clienteRepository.findById(id).get();  
    }  
    
    public Cliente getClienteByNomeAndCpf(String nome ,String cpf) {  
        return clienteRepository.findByNomeAndCpf(nome,cpf);  
    } 
   
    public Cliente getClienteByCpf(String cpf) {  
        return clienteRepository.findByCpf(cpf);  
    } 
      
    public Cliente save(Cliente cliente) {
        return clienteRepository.save(cliente);  
    }  

    public void delete(int id) {  
        clienteRepository.deleteById(id);  
    }  

    public Cliente update(Cliente cliente, int id) {  
        Cliente clientInDb = clienteRepository.findById(id).get();
        clientInDb.setNome(cliente.getNome());
        clientInDb.setDataNascimento(cliente.getDataNascimento());
        return clienteRepository.save(clientInDb);  
    }  

}
