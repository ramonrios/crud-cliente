# Platform Builders 

Este readme tem o propósito de instruir a execução do serviço de CRUD de cliente.

## Build 

Para iniciar, execute `mvn package` para executar o build da aplicação.

## Docker Compose

Após o build ser concluído, execute o comando `docker-compose up` (Necessário ter [docker]() (https://docs.docker.com/get-docker/) instalado na sua máquina). O comando iniciará a base de dados e o serviço.

## Postman

Para efeitos de uso e teste, há um arquivo postman nesse diretório para testagem.