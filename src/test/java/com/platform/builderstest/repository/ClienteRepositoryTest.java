package com.platform.builderstest.repository;

import java.util.List;
import com.platform.builderstest.model.Cliente;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import static org.assertj.core.api.Assertions.assertThat;


@RunWith(SpringRunner.class)
@DataJpaTest
public class ClienteRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private ClienteRepository clienteRepository;

    @Test
    public void whenFindByCpf_thenReturnCliente() {
        // given
        Cliente alex = new Cliente();
        alex.setCpf("08298473812");
        alex.setNome("Alex");
        entityManager.persist(alex);
        entityManager.flush();
    
        // when
        Cliente found = clienteRepository.findByCpf(alex.getCpf());
    
        // then
        assertThat(found.getCpf()).isEqualTo(alex.getCpf());

    }

    @Test
    public void whenFindById_thenReturnCliente() {
        Cliente cliente = new Cliente("test", "12346589121", "01/01/2021");
        entityManager.persistAndFlush(cliente);

        Cliente fromDb = clienteRepository.findById(cliente.getId()).orElse(null);
        assertThat(fromDb.getNome()).isEqualTo(cliente.getNome());
    }

    @Test
    public void whenFindByNomeAndCpf_thenReturnCliente() {
        Cliente cliente = new Cliente("test", "12346589121", "01/01/2021");
        entityManager.persistAndFlush(cliente);

        Cliente fromDb = clienteRepository.findByNomeAndCpf(cliente.getNome(),cliente.getCpf());
        assertThat(fromDb.getNome()).isEqualTo(cliente.getNome());
    }


    @Test
    public void givenSetOfClientes_whenFindAll_thenReturnAllClientes() {
        Cliente alex = new Cliente("alex","12345893812", "01/10/2010");
        Cliente ron = new Cliente("ron" , "78491829372", "15/10/2001");
        Cliente bob = new Cliente("bob" , "12345948371", "18/09/2000");

        entityManager.persist(alex);
        entityManager.persist(bob);
        entityManager.persist(ron);
        entityManager.flush();

        List<Cliente> allClientes = clienteRepository.findAll();

        assertThat(allClientes).hasSize(3).extracting(Cliente::getNome).containsOnly(alex.getNome(), ron.getNome(), bob.getNome());
    }

}
