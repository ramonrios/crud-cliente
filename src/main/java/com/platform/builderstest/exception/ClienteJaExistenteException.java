package com.platform.builderstest.exception;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)

public class ClienteJaExistenteException extends RuntimeException {

  public ClienteJaExistenteException(String exception) {
		  super(exception);
	}
}