package com.platform.builderstest.controller;

import java.util.List;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.platform.builderstest.model.Cliente;  
import com.platform.builderstest.service.ClienteService;  
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.net.URI;
import java.util.Optional;
import javax.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.hateoas.EntityModel;
import com.platform.builderstest.exception.*;


@RestController  
public class ClienteController {
    
    private Cliente calculateIdade(Cliente cliente){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d/MM/yyyy");
        LocalDate hoje = LocalDate.now(); 
        LocalDate dataNascimento = LocalDate.parse(cliente.getDataNascimento(), formatter);
        Period p = Period.between(dataNascimento, hoje);
        cliente.setIdade(p.getYears());
        return cliente;
    }

    @Autowired
    ClienteService clienteService;

    @GetMapping("/clientes")

    private Page <Cliente> getAllClientes(@RequestParam(name = "page" , defaultValue = "0") int page) {
        List<Cliente> clientes = new ArrayList<Cliente>();  
        clienteService.getAllClientes().forEach(cliente -> clientes.add(calculateIdade(cliente)));
        return new PageImpl<Cliente>(clientes, PageRequest.of(page, Integer.MAX_VALUE), clientes.size());
    }

    @GetMapping("/cliente")
    private EntityModel<Cliente> getCliente(@RequestParam String nome ,@RequestParam String cpf) {
        Optional<Cliente> clienteOptional = Optional.ofNullable(clienteService.getClienteByNomeAndCpf(nome, cpf)); 
        if (!clienteOptional.isPresent())
            throw new ClienteNaoEncontradoException("Cliente com nome "+ nome + " e cpf " + cpf + " Não encontrados");
           
        EntityModel<Cliente> resource = EntityModel.of(calculateIdade(clienteOptional.get()));

        return resource;    
    }  


    @PostMapping("/cliente")
    private ResponseEntity<Object> saveCliente(@Valid @RequestBody Cliente cliente) {

        Optional<Cliente> clienteOptional = Optional.ofNullable(clienteService.getClienteByCpf(cliente.getCpf()));

        if (clienteOptional.isPresent())
            throw new ClienteJaExistenteException("Cliente com cpf " + cliente.getCpf() + " já existente na base de dados");

        
        Cliente clienteSalvo = clienteService.save(cliente);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
        .buildAndExpand(clienteSalvo.getId()).toUri();

        return ResponseEntity.created(location).build();
    }

    @PutMapping("/cliente/{clienteId}")
    private ResponseEntity<Object> update(@PathVariable("clienteId") int clienteId, @RequestBody Cliente cliente){  
        Optional<Cliente> clienteOptional = Optional.ofNullable(clienteService.getClienteById(clienteId));

		if (!clienteOptional.isPresent())
        throw new ClienteNaoEncontradoException("Cliente com nome "+ cliente.getNome() + " e cpf " + cliente.getCpf() + " Não encontrados");
      

		cliente.setId(clienteId);
		
		clienteService.update(cliente, clienteId);

		return ResponseEntity.ok().build();
    }

    @PatchMapping("/cliente/{clienteId}")
    private ResponseEntity<Object> patch(@PathVariable("clienteId") int clienteId, @RequestBody Cliente cliente){  
        Optional<Cliente> clienteOptional = Optional.ofNullable(clienteService.getClienteById(clienteId));

		if (!clienteOptional.isPresent())
        throw new ClienteNaoEncontradoException("Cliente com nome "+ cliente.getNome() + " e cpf " + cliente.getCpf() + " Não encontrados");
      

		cliente.setId(clienteId);
		
		clienteService.update(cliente, clienteId);

		return ResponseEntity.ok().build();
    }

    @DeleteMapping("/cliente/{clienteId}")
    private ResponseEntity<Object> deleteCliente(@PathVariable("clienteId") int clienteId) {
      clienteService.delete(clienteId);
      return ResponseEntity.noContent().build();
    }
}  