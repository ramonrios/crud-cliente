package com.platform.builderstest.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.platform.builderstest.model.Cliente;


public interface ClienteRepository extends JpaRepository<Cliente,Integer> {
    
    public Cliente findByNomeAndCpf(String nome, String cpf);
    public Cliente findByCpf(String cpf);
}
