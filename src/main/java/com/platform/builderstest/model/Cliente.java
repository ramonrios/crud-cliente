package com.platform.builderstest.model;
import java.io.Serializable;
import javax.persistence.Column;  
import javax.persistence.GeneratedValue;  
import javax.persistence.GenerationType;  
import javax.persistence.Entity;  
import javax.persistence.Id;  
import javax.persistence.Table;
import java.io.Serializable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table
public class Cliente implements Serializable{
   
   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   private Integer id;
   @Column
   @NotNull(message="O cliente precisa de um nome")
   private String nome;
   @Column(unique=true)
   @NotNull
	@Size(min=11, message="CPF necessita de 11 dígitos")
   private String cpf;
   @Column
   private String dataNascimento;
   private transient int idade;

   public Cliente (String nome, String cpf, String dataNascimento){
      this.nome = nome;
      this.cpf = cpf;
      this.dataNascimento = dataNascimento;
   } 

   public Cliente (){

   } 

   public Integer getId() {
      return id;
   }

   public void setId(Integer id) {
      this.id = id;
   }

   public String getNome() {
      return nome;
   }

   public void setNome(String nome) {
      this.nome = nome;
   }

   public String getCpf() {
      return cpf;
   }

   public void setCpf(String cpf) {
      this.cpf = cpf;
   }

   public String getDataNascimento() {
      return dataNascimento;
   }

   public void setDataNascimento(String dataNascimento) {
      this.dataNascimento = dataNascimento;
   }

   public int getIdade() {
      return idade;
   }

   public void setIdade(int idade) {
      this.idade = idade;
   }

}