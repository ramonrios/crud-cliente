package com.platform.builderstest.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.internal.verification.VerificationModeFactory;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import com.platform.builderstest.service.ClienteService;
import com.platform.builderstest.model.Cliente;

import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;



@RunWith(SpringRunner.class)
@WebMvcTest(value = ClienteController.class, excludeAutoConfiguration = SecurityAutoConfiguration.class)
public class ClienteControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private ClienteService service;

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void whenPostCliente_thenCreateCliente() throws Exception {
        Cliente ramon = new Cliente("ramon", "08298473812" , "06/07/1993");
        ramon.setId(1);
        given(service.save(Mockito.any())).willReturn(ramon);
        mvc.perform(post("/cliente").contentType(MediaType.APPLICATION_JSON).content(JsonUtils.toJson(ramon))).andExpect(status().isCreated());
        verify(service, VerificationModeFactory.times(1)).save(Mockito.any());
        reset(service);
    }

    @Test
    public void givenClientes_whenGetClientes_thenReturnJsonArray() throws Exception {
        Cliente alex = new Cliente("alex" ,"08298473812" , "06/07/1993");
        Cliente john = new Cliente("john" ,"12309848411" , "06/02/1991");
        Cliente bob = new Cliente("bob" , "99447716102" , "15/10/1991");

        List<Cliente> allClientes = Arrays.asList(alex, john, bob);

        given(service.getAllClientes()).willReturn(allClientes);

        mvc.perform(get("/clientes").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
        verify(service, VerificationModeFactory.times(1)).getAllClientes();
        reset(service);
    }


    @Test
    public void givenCliente_whenGetCliente_thenReturnJson() throws Exception {
        Cliente alex = new Cliente("alex" ,"08298473812" , "06/07/1993");
        MultiValueMap<String, String> params = new LinkedMultiValueMap();
        params.add("nome", "alex");
        params.add("cpf", "08298473812");
        given(service.getClienteByNomeAndCpf(Mockito.any(),Mockito.any())).willReturn(alex);

        mvc.perform(get("/cliente").contentType(MediaType.APPLICATION_JSON).queryParams(params)).andExpect(jsonPath("$.nome", is("alex")));
        verify(service, VerificationModeFactory.times(1)).getClienteByNomeAndCpf(Mockito.any(),Mockito.any());
        reset(service);
    }


    @Test
    public void givenCliente_whenClienteNotFound_thenReturnError() throws Exception {
        MultiValueMap<String, String> params = new LinkedMultiValueMap();
        params.add("nome", "alex");
        params.add("cpf", "08298473222");

        given(service.getClienteByNomeAndCpf(Mockito.any(),Mockito.any())).willReturn(null);

        mvc.perform(get("/cliente").contentType(MediaType.APPLICATION_JSON).queryParams(params)).andExpect(status().isNotFound());
        verify(service, VerificationModeFactory.times(1)).getClienteByNomeAndCpf(Mockito.any(),Mockito.any());
        reset(service);
    }

    @Test
    public void givenCliente_whenClienteDeleted_thenReturn204() throws Exception {

        MultiValueMap<String, String> params = new LinkedMultiValueMap();
        params.add("clienteId", "1");
        Mockito.doNothing().when(service).delete(Mockito.anyInt());
        mvc.perform(delete("/cliente/" + "1").params(params)).andExpect(status().is(204));
        verify(service, VerificationModeFactory.times(1)).delete(Mockito.anyInt());
        reset(service);
    }


    @Test
    public void givenCliente_whenClientePatched_thenReturn200() throws Exception {
        Cliente alex = new Cliente("alex","12309847612","09/10/2011");
        MultiValueMap<String, String> params = new LinkedMultiValueMap();
        params.add("clienteId", "1");
        given(service.getClienteById(Mockito.anyInt())).willReturn(alex);        
        given(service.update(Mockito.any(),Mockito.anyInt())).willReturn(alex);        
        mvc.perform(patch("/cliente/" + "1").contentType(MediaType.APPLICATION_JSON).content(JsonUtils.toJson(alex)).params(params)).andExpect(status().isOk());
        verify(service, VerificationModeFactory.times(1)).update(Mockito.any(),Mockito.anyInt());
        reset(service);
    }
    
    @Test
    public void givenCliente_whenClientePut_thenReturn200() throws Exception {
        Cliente alex = new Cliente("alex","12309847612","09/10/2011");
        MultiValueMap<String, String> params = new LinkedMultiValueMap();
        params.add("clienteId", "1");
        given(service.getClienteById(Mockito.anyInt())).willReturn(alex);        
        given(service.update(Mockito.any(),Mockito.anyInt())).willReturn(alex);        
        mvc.perform(put("/cliente/" + "1").contentType(MediaType.APPLICATION_JSON).content(JsonUtils.toJson(alex)).params(params)).andExpect(status().isOk());
        verify(service, VerificationModeFactory.times(1)).update(Mockito.any(),Mockito.anyInt());
        reset(service);
    }

}
