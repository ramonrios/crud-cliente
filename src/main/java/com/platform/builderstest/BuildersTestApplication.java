package com.platform.builderstest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BuildersTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(BuildersTestApplication.class, args);
	}

}
